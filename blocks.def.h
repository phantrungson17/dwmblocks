//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"^C5^ ^d^", "mpdcurrent",					1,		13},
	{"^C2^ ^d^", "uptime -p | sed 's/up //'",					60,		0},
	{"", "nettraf",					5,		14},
	{"", "battery",					10,		0},
	{"", "ssid",					0,		11},
//	{"^C6^﬙ ^d^ ", "cpuage",					3,		0},
	//{"", "updater",					700,		0},
	{"",	"volume",	0,	10},
	{"", "timedate",					20,		12},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " ";
static unsigned int delimLen = 5;
